import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {Livre} from '../models/livre';
import {LIVRELIST} from '../../mock-livre';

@Injectable({
  providedIn: 'root'
})

@Injectable({ providedIn : 'root'})
export class LivreServiceService {
  getLivre(id: number): Observable<Livre>{
    return of (LIVRELIST.find(livre => livre.id === id));
  }


}
