export interface Livre {
  id: number;
  name: string;
  auteur: string;
}
