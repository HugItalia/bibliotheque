export interface Auteur {
  id: number;
  auteur: string;
  description: string;
}
