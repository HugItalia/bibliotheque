import { Component, OnInit } from '@angular/core';
import {Auteur} from '../../models/auteur';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auteurs',
  templateUrl: './auteurs.component.html',
  styleUrls: ['./auteurs.component.scss']
})
export class AuteursComponent {


  auteurList: Auteur[] = [
    { id : 1, auteur : 'J.K.Rowling ', description : 'description' },
    { id : 2, auteur : 'George R. R. Martin ', description : 'description' },
    { id : 3, auteur : 'Anne Robillard ', description : 'description' }
  ];

  constructor(private router: Router) {  }
  showDetails(auteur) {
    this.router.navigate( ['bibliotheque/auteurs', auteur.id, 'details'] );
  }

}
