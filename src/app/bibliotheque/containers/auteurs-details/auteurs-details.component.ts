import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-auteurs-details',
  templateUrl: './auteurs-details.component.html',
  styleUrls: ['./auteurs-details.component.scss']
})
export class AuteursDetailsComponent {


  auteur = [
    { id : 1, auteur : 'J.K.Rowling ', description : 'description2' },
    { id : 2, auteur : 'George R. R. Martin ', description : 'description2' },
    { id : 3, auteur : 'Anne Robillard ', description : 'description2' }
  ];

}
