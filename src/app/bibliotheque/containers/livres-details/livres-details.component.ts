import { Component, OnInit } from '@angular/core';
import {Livre} from '../../models/livre';
import {ActivatedRoute} from '@angular/router';
import {LivreServiceService} from '../../service/livre-service.service';

@Component({
  selector: 'app-livres-details',
  templateUrl: './livres-details.component.html',
  styleUrls: ['./livres-details.component.scss']
})
export class LivresDetailsComponent implements OnInit {
  livre: Livre;

  constructor(
    private route: ActivatedRoute,
    private livreService: LivreServiceService ) { }

    ngOnInit(): void {
    this.getLivre();
    }

  getLivre(): void {
    const id = + this.route.snapshot.paramMap.get('id');
    this.livreService.getLivre(id)
      .subscribe(livre => this.livre = livre);
  }

}
