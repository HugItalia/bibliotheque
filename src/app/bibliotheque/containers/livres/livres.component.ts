import { Component, OnInit } from '@angular/core';
import { Livre } from '../../models/livre';
import {Router} from '@angular/router';
import {LIVRELIST} from "../../../mock-livre";

@Component({
  selector: 'app-livres',
  templateUrl: './livres.component.html',
  styleUrls: ['./livres.component.scss']
})
export class LivresComponent {
  livreList = LIVRELIST;
}
