import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LivresComponent} from './containers/livres/livres.component';
import {AuteursComponent} from './containers/auteurs/auteurs.component';
import {LivresDetailsComponent} from './containers/livres-details/livres-details.component';
import {AuteursDetailsComponent} from './containers/auteurs-details/auteurs-details.component';

const routes: Routes = [
  {path: 'livres', component: LivresComponent},
  {path: 'auteurs', component: AuteursComponent},
  {path: 'livre/:id/details', component: LivresDetailsComponent},
  {path: 'auteur/:id/details', component: AuteursDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BibliothequeRoutingModule {
}
