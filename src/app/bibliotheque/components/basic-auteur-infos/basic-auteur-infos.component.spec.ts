import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicAuteurInfosComponent } from './basic-auteur-infos.component';

describe('BasicAuteurInfosComponent', () => {
  let component: BasicAuteurInfosComponent;
  let fixture: ComponentFixture<BasicAuteurInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicAuteurInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicAuteurInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
