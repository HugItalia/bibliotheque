import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-basic-auteur-infos',
  templateUrl: './basic-auteur-infos.component.html',
  styleUrls: ['./basic-auteur-infos.component.scss']
})
export class BasicAuteurInfosComponent {

  @Input() auteur;

}
