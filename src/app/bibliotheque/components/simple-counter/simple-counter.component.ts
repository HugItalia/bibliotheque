import {Component, Input, OnInit} from '@angular/core';
import {interval} from 'rxjs';
import {endWith, map, reduce, take, takeWhile, tap} from 'rxjs/operators';

@Component({
  selector: 'app-simple-counter',
  templateUrl: './simple-counter.component.html',
  styleUrls: ['./simple-counter.component.scss']
})
export class SimpleCounterComponent {

  @Input() startAt;
  @Input() endAt;
  @Input() step;
  @Input() delay;
  @Input() digits;
  currentValue;

  reset() {
  /*  interval(this.delay).pipe(
      map(x => this.startAt + x * this.step),
      tap(x => console.log(x)),
      takeWhile(x => x < this.endAt),
      endWith(this.endAt),
    ).subscribe(x => this.currentValue = x); */

    /* genere un chiffre aleatoire entre 1 et nblivres */
    interval(1000).pipe(
      map(x => {
        let t = Math.floor(Math.random() * 10);
        console.log(t);
        return t;
      }),
      take(5),
      reduce((x, y) => x * y)
    );



  }

}
