import {Component, Input, OnInit} from '@angular/core';
import {Livre} from '../../models/livre';

@Component({
  selector: 'app-basic-livre-infos',
  templateUrl: './basic-livre-infos.component.html',
  styleUrls: ['./basic-livre-infos.component.scss']
})
export class BasicLivreInfosComponent {

  @Input() livre: Livre;

}
