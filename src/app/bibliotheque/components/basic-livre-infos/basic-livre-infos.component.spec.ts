import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicLivreInfosComponent } from './basic-livre-infos.component';

describe('BasicLivreInfosComponent', () => {
  let component: BasicLivreInfosComponent;
  let fixture: ComponentFixture<BasicLivreInfosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicLivreInfosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicLivreInfosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
