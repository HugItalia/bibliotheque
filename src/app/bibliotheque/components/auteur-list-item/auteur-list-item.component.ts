import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Auteur} from '../../models/auteur';

@Component({
  selector: 'app-auteur-list-item',
  templateUrl: './auteur-list-item.component.html',
  styleUrls: ['./auteur-list-item.component.scss']
})
export class AuteurListItemComponent {

  @Input() auteur;
  @Output() onAuteurClicked: EventEmitter<Auteur> = new EventEmitter();

  selectAuteur(): void {
    this.onAuteurClicked.emit(this.auteur);
  }
}
