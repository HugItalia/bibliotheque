import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuteurListItemComponent } from './auteur-list-item.component';

describe('AuteurListItemComponent', () => {
  let component: AuteurListItemComponent;
  let fixture: ComponentFixture<AuteurListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuteurListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuteurListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
