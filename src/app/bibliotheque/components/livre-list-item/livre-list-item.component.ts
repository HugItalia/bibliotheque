import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Livre} from '../../models/livre';

@Component({
  selector: 'app-livre-list-item',
  templateUrl: './livre-list-item.component.html',
  styleUrls: ['./livre-list-item.component.scss']
})
export class LivreListItemComponent {
  @Input() livre : Livre;
}
