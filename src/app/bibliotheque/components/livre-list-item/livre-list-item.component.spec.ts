import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LivreListItemComponent } from './livre-list-item.component';

describe('LivreListItemComponent', () => {
  let component: LivreListItemComponent;
  let fixture: ComponentFixture<LivreListItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LivreListItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LivreListItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
