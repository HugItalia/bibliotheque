import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {BibliothequeRoutingModule} from './bibliotheque-routing.module';
import {LivresComponent} from './containers/livres/livres.component';
import {LivresDetailsComponent} from './containers/livres-details/livres-details.component';
import {AuteursDetailsComponent} from './containers/auteurs-details/auteurs-details.component';
import {AuteursComponent} from './containers/auteurs/auteurs.component';
import {BasicLivreInfosComponent} from './components/basic-livre-infos/basic-livre-infos.component';
import {LivreListItemComponent} from './components/livre-list-item/livre-list-item.component';
import {AuteurListItemComponent} from './components/auteur-list-item/auteur-list-item.component';
import {BasicAuteurInfosComponent} from './components/basic-auteur-infos/basic-auteur-infos.component';
import {SimpleCounterComponent} from './components/simple-counter/simple-counter.component';

@NgModule({
  declarations: [LivresComponent, LivresDetailsComponent, AuteursDetailsComponent, AuteursComponent, BasicLivreInfosComponent, LivreListItemComponent, AuteurListItemComponent, BasicAuteurInfosComponent, SimpleCounterComponent],
  imports: [
    CommonModule,
    BibliothequeRoutingModule
  ]
})
export class BibliothequeModule { }
