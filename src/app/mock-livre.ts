import {Livre} from './bibliotheque/models/livre';

export const LIVRELIST: Livre[] = [

{ id : 1, name : 'Harry Potter  ', auteur : 'J.K.Rowling' },
{ id : 2, name : 'Game Of Thrones  ', auteur : 'George R. R. Martin' },
{ id : 3, name : 'Chevalier d\'émeraude  ', auteur : 'Anne Robillard' }

]
